package com.bigfans.searchservice.model;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import java.util.Date;

@Data
public class AttributeValue extends AbstractModel {

	private static final long serialVersionUID = -495340455594785736L;

	protected String optionName;
	protected String optionId;
	protected String value;
	protected String categoryId;

	@Override
	public String toString() {
		return "AttributeValue [optionId=" + optionId + ", value=" + value + ", id=" + id + "]";
	}

	@Override
	public String getModule() {
		return null;
	}
}
