package com.bigfans.model.dto.cart;

import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lichong
 * @create 2018-02-16 下午2:36
 **/
@Data
public class CartPricingResultDto {

    private Map<String , List<CartItemPromotionDto>> promotionMap = new HashMap<>();
    private Map<String , List<CartItemCouponDto>> couponMap = new HashMap<>();
    private Map<String , CartItemPricingResultDto> priceMap = new HashMap<>();

    private BigDecimal originalTotalPrice = BigDecimal.ZERO;
    private BigDecimal totalPrice = BigDecimal.ZERO;

    public List<CartItemPromotionDto> getPromotion(String prodId) {
        return promotionMap == null ? null : promotionMap.get(prodId);
    }

    public CartItemPricingResultDto getPrice(String prodId){
        return priceMap.get(prodId);
    }

    public void addItemResult(CartItemPricingResultDto itemPricingResultDto){
        priceMap.put(itemPricingResultDto.getProdId() , itemPricingResultDto);
    }

    public void addPromotion(String prodId , CartItemPromotionDto promotionDto){
        List<CartItemPromotionDto> promotionDtoList = promotionMap.get(prodId);
        if(promotionDtoList == null){
            promotionDtoList = new ArrayList<>();
        }
        promotionDtoList.add(promotionDto);
        promotionMap.put(prodId , promotionDtoList);
    }

    public void addCoupon(String prodId , CartItemCouponDto couponDto){
        List<CartItemCouponDto> couponDtoList = couponMap.get(prodId);
        if(couponDtoList == null){
            couponDtoList = new ArrayList<>();
        }
        couponDtoList.add(couponDto);
        couponMap.put(prodId , couponDtoList);
    }

    public List<CartItemCouponDto> getCoupon(String prodId) {
        return couponMap == null ? null : couponMap.get(prodId);
    }
}
