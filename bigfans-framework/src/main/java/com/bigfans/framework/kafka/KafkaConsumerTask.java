package com.bigfans.framework.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class KafkaConsumerTask {

    private String topic;
    private List<KafkaConsumerRecordProcessor> processors = new ArrayList<>();

    public KafkaConsumerTask(String topic , List<KafkaListenerBean> listenerBeans) {
        for(KafkaListenerBean listenerBean : listenerBeans){
            KafkaConsumerRecordProcessor processor = new KafkaConsumerRecordProcessor(listenerBean);
            this.processors.add(processor);
        }
    }

    public void processRecord(ConsumerRecord<String, String> record){
        processors.forEach((processor) -> {
            processor.process(record);
        });
    }

    public void stop(){
        processors.forEach((processor) -> {
            processor.shutdown();
        });
    }
}
