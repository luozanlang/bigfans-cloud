package com.bigfans.framework.es.schema;

import org.elasticsearch.common.settings.Settings.Builder;

public class IndexSettingsAnalyzer_Pinyin implements ElasticSchema {
	
	public static final String NAME = "pinyin_analyzer";

	@Override
	public void build(Builder settingsBuilder) {
		// tokenizer
		settingsBuilder.put("index.analysis.tokenizer.custom_pinyin.type", "pinyin");
		settingsBuilder.put("index.analysis.tokenizer.custom_pinyin.lowercase", "true");
		settingsBuilder.put("index.analysis.tokenizer.custom_pinyin.keep_joined_full_pinyin", "true");
		settingsBuilder.put("index.analysis.tokenizer.custom_pinyin.keep_full_pinyin", "true");
		settingsBuilder.put("index.analysis.tokenizer.custom_pinyin.keep_original", "false");
		settingsBuilder.put("index.analysis.tokenizer.custom_pinyin.keep_separate_first_letter", "false");
		settingsBuilder.put("index.analysis.tokenizer.custom_pinyin.limit_first_letter_length", "16");
		// analyzer
		settingsBuilder.put("index.analysis.analyzer.pinyin_analyzer.tokenizer", "custom_pinyin");
		settingsBuilder.putList("index.analysis.analyzer.pinyin_analyzer.filter", "2_10_ngram_filter");
	}
}
