package com.bigfans.catalogservice.api;

import com.bigfans.catalogservice.model.AttributeOption;
import com.bigfans.catalogservice.model.AttributeValue;
import com.bigfans.catalogservice.service.attribute.AttributeOptionService;
import com.bigfans.framework.utils.StringHelper;
import com.bigfans.framework.web.BaseController;
import com.bigfans.framework.web.RestResponse;
import com.bigfans.catalogservice.service.attribute.AttributeValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
public class AttributeApi extends BaseController {

    @Autowired
    private AttributeValueService attributeValueService;
    @Autowired
    private AttributeOptionService attributeOptionService;

    @GetMapping(value = "/attribute")
    public RestResponse listAttributeValues(@RequestParam(value = "optionId") String optionId) throws Exception {
        List<AttributeValue> attributeValues = attributeValueService.listByOptionId(optionId);
        RestResponse resp = RestResponse.ok();
        return resp;
    }

    @GetMapping(value = "/attributes")
    public RestResponse listAttributes(
            @RequestParam(value = "prodId" , required = false) String prodId,
            @RequestParam(value = "ids" , required = false) String ids
    ) throws Exception {
        List<AttributeValue> attributes = null;
        if (StringHelper.isNotEmpty(prodId)) {
            attributes = attributeValueService.listByProduct(prodId);
        } else if (StringHelper.isNotEmpty(ids)) {
            String[] idArray = ids.split(",");
            attributes = attributeValueService.listById(Arrays.asList(idArray));
        }
        return RestResponse.ok(attributes);
    }
}
