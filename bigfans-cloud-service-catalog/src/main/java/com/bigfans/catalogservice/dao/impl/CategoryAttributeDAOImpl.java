package com.bigfans.catalogservice.dao.impl;

import com.bigfans.catalogservice.dao.CategoryAttributeDAO;
import com.bigfans.catalogservice.model.CategoryAttribute;
import com.bigfans.framework.dao.MybatisDAOImpl;
import org.springframework.stereotype.Repository;

@Repository(CategoryAttributeDAOImpl.BEAN_NAME)
public class CategoryAttributeDAOImpl extends MybatisDAOImpl<CategoryAttribute> implements CategoryAttributeDAO {

	public static final String BEAN_NAME = "categoryAttributeDAO";
	
}
