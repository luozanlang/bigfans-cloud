package com.bigfans.catalogservice.dao;

import com.bigfans.catalogservice.model.Tag;
import com.bigfans.framework.dao.BaseDAO;

import java.util.List;

public interface TagDAO extends BaseDAO<Tag>{

	List<Tag> listByProdId(String prodId);
	
	List<Tag> listByContent(String name);
	
	int updatePgRelatedDuplicates(List<String> oldTagIds, String newId);

	Integer increaseRelatedCount(String tagId, Integer increase);
}
