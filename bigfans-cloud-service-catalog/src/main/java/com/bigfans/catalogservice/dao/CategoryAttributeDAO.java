package com.bigfans.catalogservice.dao;

import com.bigfans.catalogservice.model.CategoryAttribute;
import com.bigfans.framework.dao.BaseDAO;

public interface CategoryAttributeDAO extends BaseDAO<CategoryAttribute>{

}
