package com.bigfans.catalogservice.dao;

import com.bigfans.catalogservice.model.SKU;
import com.bigfans.framework.dao.BaseDAO;

import java.util.List;

/**
 * 
 * @Description:
 * @author lichong 2015年5月31日下午9:11:57
 *
 */
public interface SkuDAO extends BaseDAO<SKU> {

	List<SKU> listByPgId(String pgId);

	SKU getByValKey(String valKey);

	SKU getByPid(String prodId);

}
