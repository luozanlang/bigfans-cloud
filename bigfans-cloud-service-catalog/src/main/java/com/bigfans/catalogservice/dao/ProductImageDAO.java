package com.bigfans.catalogservice.dao;

import com.bigfans.catalogservice.model.ImageGroup;
import com.bigfans.catalogservice.model.ProductImage;
import com.bigfans.framework.dao.BaseDAO;

import java.util.List;

/**
 * 
 * @Description:
 * @author lichong
 * 2015年1月5日上午9:03:11
 *
 */
public interface ProductImageDAO extends BaseDAO<ProductImage>{

	List<ImageGroup> listWithGroup(String pid);
	
	/**
	 * 用第一张中图作为商品默认缩略图
	 * @param prodId
	 * @return
	 */
	ProductImage getThumb(String prodId);
	
}
