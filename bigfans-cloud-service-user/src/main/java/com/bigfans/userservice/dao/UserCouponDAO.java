package com.bigfans.userservice.dao;

import com.bigfans.framework.dao.BaseDAO;
import com.bigfans.userservice.model.UserCoupon;

public interface UserCouponDAO extends BaseDAO<UserCoupon> {

    int use(String userId, String couponId);

    int getOwnedCount(String userId , String couponId);

    int addCount(String userId , String couponId , int count);
}
