package com.bigfans.orderservice.api.clients;

import com.bigfans.Constants;
import com.bigfans.framework.utils.BeanUtils;
import com.bigfans.framework.utils.JsonUtils;
import com.bigfans.framework.web.CookieHolder;
import com.bigfans.framework.web.RestResponse;
import com.bigfans.orderservice.model.Product;
import com.bigfans.orderservice.model.ProductSpec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * @author lichong
 * @create 2018-02-13 下午7:42
 **/
@Component
public class CatalogServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    public CompletableFuture<Product> getProductById(String prodId) {
        return CompletableFuture.supplyAsync(() -> {
            UriComponents builder = UriComponentsBuilder.fromUriString("http://catalog-service/product/{prodId}").build().expand(prodId).encode();
            ResponseEntity<RestResponse> responseEntity = restTemplate.getForEntity(builder.toUri(), RestResponse.class);
            RestResponse response = responseEntity.getBody();
            Map data = (Map) response.getData();
            List specList = (List)data.get("specList");
            List<ProductSpec> specs = new ArrayList<>();
            for(int i = 0;i<specList.size();i++){
                ProductSpec spec = BeanUtils.mapToModel((Map)specList.get(i) , ProductSpec.class);
                specs.add(spec);
            }
            Product product = BeanUtils.mapToModel((Map) response.getData(), Product.class);
            product.setSpecs(JsonUtils.toJsonString(specs));
            return product;
        });
    }

    public CompletableFuture<String> orderStockOut(String orderId, Map<String, Integer> prodQuantityMap) {
        String userToken = CookieHolder.getValue(Constants.TOKEN.KEY_NAME);
        return CompletableFuture.supplyAsync(() -> {
            HttpHeaders headers = new HttpHeaders();
            headers.put(Constants.TOKEN.HEADER_KEY_NAME, Arrays.asList(Constants.TOKEN.KEY_NAME + "=" + userToken));
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));

            HttpEntity requestEntity = new HttpEntity(prodQuantityMap, headers);

            UriComponents builder = UriComponentsBuilder.fromUriString("http://catalog-service/orderStockOut?id={id}").build().expand(orderId).encode();
            ResponseEntity<RestResponse> responseEntity = restTemplate.exchange(builder.toUri(), HttpMethod.POST, requestEntity, RestResponse.class);
            RestResponse restResponse = responseEntity.getBody();
            return null;
        });
    }

    public CompletableFuture<List<ProductSpec>> getSpecsByProductId(String prodId) {
        return CompletableFuture.supplyAsync(() -> {
            UriComponents builder = UriComponentsBuilder.fromUriString("http://catalog-service/specs?prodId={prodId}").build().expand(prodId).encode();
            ResponseEntity<RestResponse> responseEntity = restTemplate.getForEntity(builder.toUri(), RestResponse.class);
            RestResponse response = responseEntity.getBody();
            List list = (List) response.getData();
            List<ProductSpec> specs = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                ProductSpec productSpec = BeanUtils.mapToModel((Map) list.get(i), ProductSpec.class);
                specs.add(productSpec);
            }
            return specs;
        });
    }

    public CompletableFuture<String> getTagsByProductId(String prodId) {
        return CompletableFuture.supplyAsync(() -> {
            UriComponents builder = UriComponentsBuilder.fromUriString("http://catalog-service/tags?prodId={prodId}").build().expand(prodId).encode();
            ResponseEntity<String> responseEntity = restTemplate.getForEntity(builder.toUri(), String.class);
            String jsonData = responseEntity.getBody();
            return jsonData;
        });
    }
}
