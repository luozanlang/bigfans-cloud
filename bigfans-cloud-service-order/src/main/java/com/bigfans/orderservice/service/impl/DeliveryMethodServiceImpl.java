package com.bigfans.orderservice.service.impl;

import com.bigfans.framework.dao.BaseServiceImpl;
import com.bigfans.framework.dao.ParameterMap;
import com.bigfans.orderservice.dao.DeliveryMethodDAO;
import com.bigfans.orderservice.model.DeliveryMethod;
import com.bigfans.orderservice.service.DeliveryMethodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 
 * @Description:配送类型
 * @author lichong
 * 2015年4月2日上午11:30:07
 *
 */
@Service(DeliveryMethodServiceImpl.BEAN_NAME)
public class DeliveryMethodServiceImpl extends BaseServiceImpl<DeliveryMethod> implements DeliveryMethodService {

	public static final String BEAN_NAME = "deliveryMethodService";
	
	private DeliveryMethodDAO deliveryMethodDAO;
	
	@Autowired
	public DeliveryMethodServiceImpl(DeliveryMethodDAO deliveryMethodDAO) {
		super(deliveryMethodDAO);
		this.deliveryMethodDAO = deliveryMethodDAO;
	}

	public List<DeliveryMethod> listAllAvailable() throws Exception {
		return deliveryMethodDAO.list(new ParameterMap(), 0L, 10L);
	}
	
}
