package com.bigfans.paymentservice.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @Description:订单条目
 * @author lichong 2014年12月15日下午11:34:27
 *
 */
@Data
public class OrderItem {

	private static final long serialVersionUID = -112379520656314767L;
	// 未评价
	public static final int COMMENT_STATUS_UNCOMMENTED = 0;
	// 已评价
	public static final int COMMENT_STATUS_COMMENTED = 1;

	protected String id;
	protected Date createDate;
	protected Date updateDate;
	protected String orderId;
	protected String userId;
	/* 产品id */
	protected String prodId;
	protected String prodName;
	/* 产品成交价 */
	protected BigDecimal dealPrice;
	/* 产品成交总价 */
	protected BigDecimal dealSubTotal;
	/* 购买数量 */
	protected Integer quantity;

	public String getModule() {
		return "OrderItem";
	}
	
}
