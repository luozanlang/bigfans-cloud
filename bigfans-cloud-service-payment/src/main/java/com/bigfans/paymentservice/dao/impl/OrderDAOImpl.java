package com.bigfans.paymentservice.dao.impl;

import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.paymentservice.dao.OrderDAO;
import com.bigfans.paymentservice.model.Order;
import org.springframework.stereotype.Repository;


/**
 * 
 * @Description:订单Mapper
 * @author lichong 
 * 2014年12月14日下午4:05:14
 *
 */
@Repository(OrderDAOImpl.BEAN_NAME)
public class OrderDAOImpl extends MybatisDAOImpl<Order> implements OrderDAO {

	public static final String BEAN_NAME = "orderDAO";

}
